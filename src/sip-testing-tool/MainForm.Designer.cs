﻿namespace Clc.Sip
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.loginButton = new System.Windows.Forms.Button();
            this.serverTextBox = new System.Windows.Forms.TextBox();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.aoTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.apTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.patronInformationContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.noneMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.holdItemsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overdueItemsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargedItemsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fineItemsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recallItemsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unavailableHoldsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.locationCodeTextBox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.vendorProfileComboBox = new System.Windows.Forms.ComboBox();
            this.renewAllButton = new System.Windows.Forms.Button();
            this.renewButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bpTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.patronBarcodeTextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bqTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.patronPinTextBox = new System.Windows.Forms.TextBox();
            this.checkoutButton = new System.Windows.Forms.Button();
            this.patronStatusButton = new System.Windows.Forms.Button();
            this.checkInButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.itemBarcodeTextBox = new System.Windows.Forms.TextBox();
            this.endSessionButton = new System.Windows.Forms.Button();
            this.itemInformationButton = new System.Windows.Forms.Button();
            this.patronInformationButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.noticeUpdateButton = new System.Windows.Forms.Button();
            this.feePaidButton = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.transactionIdTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.feeIdentifierTextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.paymentTypeTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.feeTypeTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.importSettingsButton = new System.Windows.Forms.Button();
            this.exportSettingsButton = new System.Windows.Forms.Button();
            this.exportLogButton = new System.Windows.Forms.Button();
            this.clearLogButton = new System.Windows.Forms.Button();
            this.reloadDefaultSettingsButton = new System.Windows.Forms.Button();
            this.changeDefaultSettingsButton = new System.Windows.Forms.Button();
            this.logRichTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.patronInformationContextMenu.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(83, 15);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(125, 20);
            this.usernameTextBox.TabIndex = 0;
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(83, 40);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(125, 20);
            this.passwordTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "SIP Username";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "SIP Password";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(83, 116);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(64, 23);
            this.loginButton.TabIndex = 4;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // serverTextBox
            // 
            this.serverTextBox.Location = new System.Drawing.Point(70, 19);
            this.serverTextBox.Name = "serverTextBox";
            this.serverTextBox.Size = new System.Drawing.Size(107, 20);
            this.serverTextBox.TabIndex = 0;
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(70, 42);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(107, 20);
            this.portTextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "SIP Server";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "SIP Port";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.aoTextBox);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.connectButton);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.apTextBox);
            this.groupBox1.Controls.Add(this.serverTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.portTextBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(180, 147);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection Information";
            // 
            // aoTextBox
            // 
            this.aoTextBox.Location = new System.Drawing.Point(70, 66);
            this.aoTextBox.Name = "aoTextBox";
            this.aoTextBox.Size = new System.Drawing.Size(107, 20);
            this.aoTextBox.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(42, 69);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "AO";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(53, 116);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 4;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(43, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "AP";
            // 
            // apTextBox
            // 
            this.apTextBox.Location = new System.Drawing.Point(70, 90);
            this.apTextBox.Name = "apTextBox";
            this.apTextBox.Size = new System.Drawing.Size(107, 20);
            this.apTextBox.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Vendor Profile";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // patronInformationContextMenu
            // 
            this.patronInformationContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.noneMenuItem,
            this.holdItemsMenuItem,
            this.overdueItemsMenuItem,
            this.chargedItemsMenuItem,
            this.fineItemsMenuItem,
            this.recallItemsMenuItem,
            this.unavailableHoldsMenuItem});
            this.patronInformationContextMenu.Name = "patronInformationContextMenu";
            this.patronInformationContextMenu.Size = new System.Drawing.Size(170, 158);
            // 
            // noneMenuItem
            // 
            this.noneMenuItem.Name = "noneMenuItem";
            this.noneMenuItem.Size = new System.Drawing.Size(169, 22);
            this.noneMenuItem.Text = "None";
            this.noneMenuItem.Click += new System.EventHandler(this.noneMenuItem_Click);
            // 
            // holdItemsMenuItem
            // 
            this.holdItemsMenuItem.Name = "holdItemsMenuItem";
            this.holdItemsMenuItem.Size = new System.Drawing.Size(169, 22);
            this.holdItemsMenuItem.Text = "Hold Items";
            this.holdItemsMenuItem.Click += new System.EventHandler(this.holdItemsMenuItem_Click);
            // 
            // overdueItemsMenuItem
            // 
            this.overdueItemsMenuItem.Name = "overdueItemsMenuItem";
            this.overdueItemsMenuItem.Size = new System.Drawing.Size(169, 22);
            this.overdueItemsMenuItem.Text = "Overdue Items";
            this.overdueItemsMenuItem.Click += new System.EventHandler(this.overdueItemsMenuItem_Click);
            // 
            // chargedItemsMenuItem
            // 
            this.chargedItemsMenuItem.Name = "chargedItemsMenuItem";
            this.chargedItemsMenuItem.Size = new System.Drawing.Size(169, 22);
            this.chargedItemsMenuItem.Text = "Charged Items";
            this.chargedItemsMenuItem.Click += new System.EventHandler(this.chargedItemsMenuItem_Click);
            // 
            // fineItemsMenuItem
            // 
            this.fineItemsMenuItem.Name = "fineItemsMenuItem";
            this.fineItemsMenuItem.Size = new System.Drawing.Size(169, 22);
            this.fineItemsMenuItem.Text = "Fine Items";
            this.fineItemsMenuItem.Click += new System.EventHandler(this.fineItemsMenuItem_Click);
            // 
            // recallItemsMenuItem
            // 
            this.recallItemsMenuItem.Name = "recallItemsMenuItem";
            this.recallItemsMenuItem.Size = new System.Drawing.Size(169, 22);
            this.recallItemsMenuItem.Text = "Recall Items";
            this.recallItemsMenuItem.Click += new System.EventHandler(this.recallItemsMenuItem_Click);
            // 
            // unavailableHoldsMenuItem
            // 
            this.unavailableHoldsMenuItem.Name = "unavailableHoldsMenuItem";
            this.unavailableHoldsMenuItem.Size = new System.Drawing.Size(169, 22);
            this.unavailableHoldsMenuItem.Text = "Unavailable Holds";
            this.unavailableHoldsMenuItem.Click += new System.EventHandler(this.unavailableHoldsMenuItem_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.locationCodeTextBox);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.vendorProfileComboBox);
            this.groupBox5.Controls.Add(this.usernameTextBox);
            this.groupBox5.Controls.Add(this.loginButton);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.passwordTextBox);
            this.groupBox5.Location = new System.Drawing.Point(198, 7);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(214, 147);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Login Information";
            // 
            // locationCodeTextBox
            // 
            this.locationCodeTextBox.Location = new System.Drawing.Point(83, 90);
            this.locationCodeTextBox.Name = "locationCodeTextBox";
            this.locationCodeTextBox.Size = new System.Drawing.Size(125, 20);
            this.locationCodeTextBox.TabIndex = 3;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(2, 94);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "Location Code";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // vendorProfileComboBox
            // 
            this.vendorProfileComboBox.FormattingEnabled = true;
            this.vendorProfileComboBox.Items.AddRange(new object[] {
            "Envisionware",
            "Comprise",
            "3M",
            "ITG"});
            this.vendorProfileComboBox.Location = new System.Drawing.Point(83, 65);
            this.vendorProfileComboBox.Name = "vendorProfileComboBox";
            this.vendorProfileComboBox.Size = new System.Drawing.Size(125, 21);
            this.vendorProfileComboBox.TabIndex = 2;
            // 
            // renewAllButton
            // 
            this.renewAllButton.Location = new System.Drawing.Point(951, 60);
            this.renewAllButton.Name = "renewAllButton";
            this.renewAllButton.Size = new System.Drawing.Size(75, 34);
            this.renewAllButton.TabIndex = 9;
            this.renewAllButton.Text = "Renew All Items";
            this.renewAllButton.UseVisualStyleBackColor = true;
            this.renewAllButton.Click += new System.EventHandler(this.renewAllButton_Click);
            // 
            // renewButton
            // 
            this.renewButton.Location = new System.Drawing.Point(870, 98);
            this.renewButton.Name = "renewButton";
            this.renewButton.Size = new System.Drawing.Size(75, 34);
            this.renewButton.TabIndex = 11;
            this.renewButton.Text = "Renew   Item";
            this.renewButton.UseVisualStyleBackColor = true;
            this.renewButton.Click += new System.EventHandler(this.renewButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bpTextBox);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.patronBarcodeTextBox);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.bqTextBox);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.patronPinTextBox);
            this.groupBox2.Location = new System.Drawing.Point(417, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(178, 89);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Patron Information";
            // 
            // bpTextBox
            // 
            this.bpTextBox.Location = new System.Drawing.Point(60, 60);
            this.bpTextBox.Name = "bpTextBox";
            this.bpTextBox.Size = new System.Drawing.Size(40, 20);
            this.bpTextBox.TabIndex = 2;
            this.bpTextBox.Text = "1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "BP";
            // 
            // patronBarcodeTextBox
            // 
            this.patronBarcodeTextBox.Location = new System.Drawing.Point(60, 17);
            this.patronBarcodeTextBox.Name = "patronBarcodeTextBox";
            this.patronBarcodeTextBox.Size = new System.Drawing.Size(107, 20);
            this.patronBarcodeTextBox.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(100, 63);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "BQ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Barcode";
            // 
            // bqTextBox
            // 
            this.bqTextBox.Location = new System.Drawing.Point(127, 60);
            this.bqTextBox.Name = "bqTextBox";
            this.bqTextBox.Size = new System.Drawing.Size(40, 20);
            this.bqTextBox.TabIndex = 3;
            this.bqTextBox.Text = "5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "PIN";
            // 
            // patronPinTextBox
            // 
            this.patronPinTextBox.Location = new System.Drawing.Point(60, 38);
            this.patronPinTextBox.Name = "patronPinTextBox";
            this.patronPinTextBox.Size = new System.Drawing.Size(107, 20);
            this.patronPinTextBox.TabIndex = 1;
            // 
            // checkoutButton
            // 
            this.checkoutButton.Location = new System.Drawing.Point(870, 22);
            this.checkoutButton.Name = "checkoutButton";
            this.checkoutButton.Size = new System.Drawing.Size(75, 34);
            this.checkoutButton.TabIndex = 5;
            this.checkoutButton.Text = "Item Checkout";
            this.checkoutButton.UseVisualStyleBackColor = true;
            this.checkoutButton.Click += new System.EventHandler(this.checkoutButton_Click);
            // 
            // patronStatusButton
            // 
            this.patronStatusButton.Location = new System.Drawing.Point(789, 60);
            this.patronStatusButton.Name = "patronStatusButton";
            this.patronStatusButton.Size = new System.Drawing.Size(75, 34);
            this.patronStatusButton.TabIndex = 7;
            this.patronStatusButton.Text = "Patron Status";
            this.patronStatusButton.UseVisualStyleBackColor = true;
            this.patronStatusButton.Click += new System.EventHandler(this.patronStatusButton_Click);
            // 
            // checkInButton
            // 
            this.checkInButton.Location = new System.Drawing.Point(870, 60);
            this.checkInButton.Name = "checkInButton";
            this.checkInButton.Size = new System.Drawing.Size(75, 34);
            this.checkInButton.TabIndex = 8;
            this.checkInButton.Text = "Item Check-in";
            this.checkInButton.UseVisualStyleBackColor = true;
            this.checkInButton.Click += new System.EventHandler(this.checkInButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.itemBarcodeTextBox);
            this.groupBox3.Location = new System.Drawing.Point(418, 109);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(178, 50);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Item Information";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Barcode";
            // 
            // itemBarcodeTextBox
            // 
            this.itemBarcodeTextBox.Location = new System.Drawing.Point(60, 19);
            this.itemBarcodeTextBox.Name = "itemBarcodeTextBox";
            this.itemBarcodeTextBox.Size = new System.Drawing.Size(107, 20);
            this.itemBarcodeTextBox.TabIndex = 0;
            // 
            // endSessionButton
            // 
            this.endSessionButton.Location = new System.Drawing.Point(789, 98);
            this.endSessionButton.Name = "endSessionButton";
            this.endSessionButton.Size = new System.Drawing.Size(75, 34);
            this.endSessionButton.TabIndex = 10;
            this.endSessionButton.Text = "End  Session";
            this.endSessionButton.UseVisualStyleBackColor = true;
            this.endSessionButton.Click += new System.EventHandler(this.endSessionButton_Click);
            // 
            // itemInformationButton
            // 
            this.itemInformationButton.Location = new System.Drawing.Point(951, 98);
            this.itemInformationButton.Name = "itemInformationButton";
            this.itemInformationButton.Size = new System.Drawing.Size(75, 34);
            this.itemInformationButton.TabIndex = 12;
            this.itemInformationButton.Text = "Item Information";
            this.itemInformationButton.UseVisualStyleBackColor = true;
            this.itemInformationButton.Click += new System.EventHandler(this.itemInformationButton_Click);
            // 
            // patronInformationButton
            // 
            this.patronInformationButton.Location = new System.Drawing.Point(789, 22);
            this.patronInformationButton.Name = "patronInformationButton";
            this.patronInformationButton.Size = new System.Drawing.Size(75, 34);
            this.patronInformationButton.TabIndex = 4;
            this.patronInformationButton.Text = "Patron Information";
            this.patronInformationButton.UseVisualStyleBackColor = true;
            this.patronInformationButton.Click += new System.EventHandler(this.patronInformationButton_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 541);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1049, 22);
            this.statusStrip1.TabIndex = 28;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(88, 17);
            this.toolStripStatusLabel1.Text = "Not Connected";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.noticeUpdateButton);
            this.splitContainer1.Panel1.Controls.Add(this.feePaidButton);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox4);
            this.splitContainer1.Panel1.Controls.Add(this.label14);
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.importSettingsButton);
            this.splitContainer1.Panel1.Controls.Add(this.exportSettingsButton);
            this.splitContainer1.Panel1.Controls.Add(this.exportLogButton);
            this.splitContainer1.Panel1.Controls.Add(this.clearLogButton);
            this.splitContainer1.Panel1.Controls.Add(this.reloadDefaultSettingsButton);
            this.splitContainer1.Panel1.Controls.Add(this.changeDefaultSettingsButton);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox5);
            this.splitContainer1.Panel1.Controls.Add(this.renewAllButton);
            this.splitContainer1.Panel1.Controls.Add(this.patronInformationButton);
            this.splitContainer1.Panel1.Controls.Add(this.renewButton);
            this.splitContainer1.Panel1.Controls.Add(this.itemInformationButton);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel1.Controls.Add(this.checkoutButton);
            this.splitContainer1.Panel1.Controls.Add(this.endSessionButton);
            this.splitContainer1.Panel1.Controls.Add(this.patronStatusButton);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel1.Controls.Add(this.checkInButton);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.logRichTextBox);
            this.splitContainer1.Size = new System.Drawing.Size(1049, 541);
            this.splitContainer1.SplitterDistance = 198;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.TabStop = false;
            // 
            // noticeUpdateButton
            // 
            this.noticeUpdateButton.Location = new System.Drawing.Point(870, 138);
            this.noticeUpdateButton.Name = "noticeUpdateButton";
            this.noticeUpdateButton.Size = new System.Drawing.Size(75, 35);
            this.noticeUpdateButton.TabIndex = 2;
            this.noticeUpdateButton.Text = "Update Notice";
            this.noticeUpdateButton.UseVisualStyleBackColor = true;
            this.noticeUpdateButton.Click += new System.EventHandler(this.noticeUpdateButton_Click);
            // 
            // feePaidButton
            // 
            this.feePaidButton.Location = new System.Drawing.Point(951, 22);
            this.feePaidButton.Name = "feePaidButton";
            this.feePaidButton.Size = new System.Drawing.Size(75, 34);
            this.feePaidButton.TabIndex = 6;
            this.feePaidButton.Text = "Fee Paid";
            this.feePaidButton.UseVisualStyleBackColor = true;
            this.feePaidButton.Click += new System.EventHandler(this.feePaidButton_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.amountTextBox);
            this.groupBox4.Controls.Add(this.transactionIdTextBox);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.feeIdentifierTextBox);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.paymentTypeTextBox);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.feeTypeTextBox);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Location = new System.Drawing.Point(602, 9);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(172, 150);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Fee Information";
            // 
            // amountTextBox
            // 
            this.amountTextBox.Location = new System.Drawing.Point(85, 112);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.Size = new System.Drawing.Size(81, 20);
            this.amountTextBox.TabIndex = 4;
            // 
            // transactionIdTextBox
            // 
            this.transactionIdTextBox.Location = new System.Drawing.Point(85, 89);
            this.transactionIdTextBox.Name = "transactionIdTextBox";
            this.transactionIdTextBox.Size = new System.Drawing.Size(81, 20);
            this.transactionIdTextBox.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 115);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 5;
            this.label17.Text = "Payment Amt";
            // 
            // feeIdentifierTextBox
            // 
            this.feeIdentifierTextBox.Location = new System.Drawing.Point(85, 66);
            this.feeIdentifierTextBox.Name = "feeIdentifierTextBox";
            this.feeIdentifierTextBox.Size = new System.Drawing.Size(81, 20);
            this.feeIdentifierTextBox.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 92);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Transaction ID";
            // 
            // paymentTypeTextBox
            // 
            this.paymentTypeTextBox.Location = new System.Drawing.Point(85, 43);
            this.paymentTypeTextBox.Name = "paymentTypeTextBox";
            this.paymentTypeTextBox.Size = new System.Drawing.Size(81, 20);
            this.paymentTypeTextBox.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(13, 69);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Fee Identifier";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Payment Type";
            // 
            // feeTypeTextBox
            // 
            this.feeTypeTextBox.Location = new System.Drawing.Point(85, 19);
            this.feeTypeTextBox.Name = "feeTypeTextBox";
            this.feeTypeTextBox.Size = new System.Drawing.Size(81, 20);
            this.feeTypeTextBox.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(29, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(52, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Fee Type";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(513, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 20);
            this.label14.TabIndex = 37;
            this.label14.Text = "|";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(194, 168);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 20);
            this.label9.TabIndex = 36;
            this.label9.Text = "|";
            // 
            // importSettingsButton
            // 
            this.importSettingsButton.Location = new System.Drawing.Point(632, 167);
            this.importSettingsButton.Name = "importSettingsButton";
            this.importSettingsButton.Size = new System.Drawing.Size(96, 23);
            this.importSettingsButton.TabIndex = 35;
            this.importSettingsButton.Text = "Import Settings";
            this.importSettingsButton.UseVisualStyleBackColor = true;
            this.importSettingsButton.Click += new System.EventHandler(this.importSettingsButton_Click);
            // 
            // exportSettingsButton
            // 
            this.exportSettingsButton.Location = new System.Drawing.Point(530, 167);
            this.exportSettingsButton.Name = "exportSettingsButton";
            this.exportSettingsButton.Size = new System.Drawing.Size(96, 23);
            this.exportSettingsButton.TabIndex = 34;
            this.exportSettingsButton.Text = "Export Settings";
            this.exportSettingsButton.UseVisualStyleBackColor = true;
            this.exportSettingsButton.Click += new System.EventHandler(this.exportSettingsButton_Click);
            // 
            // exportLogButton
            // 
            this.exportLogButton.Location = new System.Drawing.Point(107, 167);
            this.exportLogButton.Name = "exportLogButton";
            this.exportLogButton.Size = new System.Drawing.Size(85, 23);
            this.exportLogButton.TabIndex = 33;
            this.exportLogButton.TabStop = false;
            this.exportLogButton.Text = "Export Log";
            this.exportLogButton.UseVisualStyleBackColor = true;
            this.exportLogButton.Click += new System.EventHandler(this.exportLogButton_Click);
            // 
            // clearLogButton
            // 
            this.clearLogButton.Location = new System.Drawing.Point(12, 167);
            this.clearLogButton.Name = "clearLogButton";
            this.clearLogButton.Size = new System.Drawing.Size(85, 23);
            this.clearLogButton.TabIndex = 32;
            this.clearLogButton.TabStop = false;
            this.clearLogButton.Text = "Clear Log";
            this.clearLogButton.UseVisualStyleBackColor = true;
            this.clearLogButton.Click += new System.EventHandler(this.clearLogButton_Click);
            // 
            // reloadDefaultSettingsButton
            // 
            this.reloadDefaultSettingsButton.Location = new System.Drawing.Point(364, 167);
            this.reloadDefaultSettingsButton.Name = "reloadDefaultSettingsButton";
            this.reloadDefaultSettingsButton.Size = new System.Drawing.Size(147, 23);
            this.reloadDefaultSettingsButton.TabIndex = 30;
            this.reloadDefaultSettingsButton.TabStop = false;
            this.reloadDefaultSettingsButton.Text = "Reload Default Values";
            this.reloadDefaultSettingsButton.UseVisualStyleBackColor = true;
            this.reloadDefaultSettingsButton.Click += new System.EventHandler(this.reloadDefaultSettingsButton_Click);
            // 
            // changeDefaultSettingsButton
            // 
            this.changeDefaultSettingsButton.Location = new System.Drawing.Point(211, 167);
            this.changeDefaultSettingsButton.Name = "changeDefaultSettingsButton";
            this.changeDefaultSettingsButton.Size = new System.Drawing.Size(147, 23);
            this.changeDefaultSettingsButton.TabIndex = 29;
            this.changeDefaultSettingsButton.TabStop = false;
            this.changeDefaultSettingsButton.Text = "Change Default Settings";
            this.changeDefaultSettingsButton.UseVisualStyleBackColor = true;
            this.changeDefaultSettingsButton.Click += new System.EventHandler(this.changeDefaultSettingsButton_Click);
            // 
            // logRichTextBox
            // 
            this.logRichTextBox.BackColor = System.Drawing.Color.White;
            this.logRichTextBox.DetectUrls = false;
            this.logRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logRichTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.logRichTextBox.Name = "logRichTextBox";
            this.logRichTextBox.ReadOnly = true;
            this.logRichTextBox.Size = new System.Drawing.Size(1049, 339);
            this.logRichTextBox.TabIndex = 1;
            this.logRichTextBox.TabStop = false;
            this.logRichTextBox.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 563);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "SIP Testing Tool";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.patronInformationContextMenu.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox usernameTextBox;
		private System.Windows.Forms.TextBox passwordTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button loginButton;
		private System.Windows.Forms.TextBox serverTextBox;
		private System.Windows.Forms.TextBox portTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ContextMenuStrip patronInformationContextMenu;
		private System.Windows.Forms.ToolStripMenuItem noneMenuItem;
		private System.Windows.Forms.ToolStripMenuItem holdItemsMenuItem;
		private System.Windows.Forms.ToolStripMenuItem overdueItemsMenuItem;
		private System.Windows.Forms.ToolStripMenuItem chargedItemsMenuItem;
		private System.Windows.Forms.ToolStripMenuItem fineItemsMenuItem;
		private System.Windows.Forms.ToolStripMenuItem recallItemsMenuItem;
		private System.Windows.Forms.ToolStripMenuItem unavailableHoldsMenuItem;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Button renewAllButton;
		private System.Windows.Forms.Button renewButton;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox patronBarcodeTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox patronPinTextBox;
		private System.Windows.Forms.Button checkoutButton;
		private System.Windows.Forms.Button patronStatusButton;
		private System.Windows.Forms.Button checkInButton;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox itemBarcodeTextBox;
		private System.Windows.Forms.Button endSessionButton;
		private System.Windows.Forms.Button itemInformationButton;
		private System.Windows.Forms.Button patronInformationButton;
		private System.Windows.Forms.Button connectButton;
		private System.Windows.Forms.TextBox aoTextBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox apTextBox;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.ComboBox vendorProfileComboBox;
		private System.Windows.Forms.Button changeDefaultSettingsButton;
		private System.Windows.Forms.Button reloadDefaultSettingsButton;
		private System.Windows.Forms.RichTextBox logRichTextBox;
		private System.Windows.Forms.Button clearLogButton;
		private System.Windows.Forms.TextBox bpTextBox;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox bqTextBox;
		private System.Windows.Forms.Button exportLogButton;
		private System.Windows.Forms.Button exportSettingsButton;
		private System.Windows.Forms.Button importSettingsButton;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Button feePaidButton;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox transactionIdTextBox;
		private System.Windows.Forms.TextBox feeIdentifierTextBox;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox paymentTypeTextBox;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox feeTypeTextBox;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox amountTextBox;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox locationCodeTextBox;
		private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button noticeUpdateButton;
    }
}

