﻿namespace Clc.Sip
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.hostnameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.aoTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.apTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.vendorProfileTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.patronBarcodeTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.patronPinTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.itemBarcodeTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.connectOnStartupCheckBox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.darkThemeCheckBox = new System.Windows.Forms.CheckBox();
            this.encodingComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.checkChecksumCheckbox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hostname";
            // 
            // hostnameTextBox
            // 
            this.hostnameTextBox.Location = new System.Drawing.Point(138, 12);
            this.hostnameTextBox.Name = "hostnameTextBox";
            this.hostnameTextBox.Size = new System.Drawing.Size(160, 20);
            this.hostnameTextBox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(104, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Port";
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(138, 38);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(160, 20);
            this.portTextBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(108, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "AO";
            // 
            // aoTextBox
            // 
            this.aoTextBox.Location = new System.Drawing.Point(138, 64);
            this.aoTextBox.Name = "aoTextBox";
            this.aoTextBox.Size = new System.Drawing.Size(160, 20);
            this.aoTextBox.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(109, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "AP";
            // 
            // apTextBox
            // 
            this.apTextBox.Location = new System.Drawing.Point(138, 90);
            this.apTextBox.Name = "apTextBox";
            this.apTextBox.Size = new System.Drawing.Size(160, 20);
            this.apTextBox.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "SIP Username";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(138, 116);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(160, 20);
            this.usernameTextBox.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "SIP Password";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(138, 142);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(160, 20);
            this.passwordTextBox.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(57, 171);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Vendor Profile";
            // 
            // vendorProfileTextBox
            // 
            this.vendorProfileTextBox.Location = new System.Drawing.Point(138, 168);
            this.vendorProfileTextBox.Name = "vendorProfileTextBox";
            this.vendorProfileTextBox.Size = new System.Drawing.Size(160, 20);
            this.vendorProfileTextBox.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(49, 197);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Patron Barcode";
            // 
            // patronBarcodeTextBox
            // 
            this.patronBarcodeTextBox.Location = new System.Drawing.Point(138, 194);
            this.patronBarcodeTextBox.Name = "patronBarcodeTextBox";
            this.patronBarcodeTextBox.Size = new System.Drawing.Size(160, 20);
            this.patronBarcodeTextBox.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(71, 223);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Patron PIN";
            // 
            // patronPinTextBox
            // 
            this.patronPinTextBox.Location = new System.Drawing.Point(138, 220);
            this.patronPinTextBox.Name = "patronPinTextBox";
            this.patronPinTextBox.Size = new System.Drawing.Size(160, 20);
            this.patronPinTextBox.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(60, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Item Barcode";
            // 
            // itemBarcodeTextBox
            // 
            this.itemBarcodeTextBox.Location = new System.Drawing.Point(138, 246);
            this.itemBarcodeTextBox.Name = "itemBarcodeTextBox";
            this.itemBarcodeTextBox.Size = new System.Drawing.Size(160, 20);
            this.itemBarcodeTextBox.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(33, 275);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Connect on startup";
            // 
            // connectOnStartupCheckBox
            // 
            this.connectOnStartupCheckBox.AutoSize = true;
            this.connectOnStartupCheckBox.Location = new System.Drawing.Point(138, 274);
            this.connectOnStartupCheckBox.Name = "connectOnStartupCheckBox";
            this.connectOnStartupCheckBox.Size = new System.Drawing.Size(15, 14);
            this.connectOnStartupCheckBox.TabIndex = 10;
            this.connectOnStartupCheckBox.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(103, 387);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Save and Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 297);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Enable dark theme?";
            // 
            // darkThemeCheckBox
            // 
            this.darkThemeCheckBox.AutoSize = true;
            this.darkThemeCheckBox.Location = new System.Drawing.Point(138, 296);
            this.darkThemeCheckBox.Name = "darkThemeCheckBox";
            this.darkThemeCheckBox.Size = new System.Drawing.Size(15, 14);
            this.darkThemeCheckBox.TabIndex = 11;
            this.darkThemeCheckBox.UseVisualStyleBackColor = true;
            // 
            // encodingComboBox
            // 
            this.encodingComboBox.FormattingEnabled = true;
            this.encodingComboBox.Items.AddRange(new object[] {
            "UTF-8",
            "ISO-8859-1"});
            this.encodingComboBox.Location = new System.Drawing.Point(136, 316);
            this.encodingComboBox.Name = "encodingComboBox";
            this.encodingComboBox.Size = new System.Drawing.Size(121, 21);
            this.encodingComboBox.TabIndex = 13;
            this.encodingComboBox.SelectedIndexChanged += new System.EventHandler(this.encodingComboBox_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(30, 319);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Character encoding";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 342);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Check return checksum?";
            // 
            // checkChecksumCheckbox
            // 
            this.checkChecksumCheckbox.AutoSize = true;
            this.checkChecksumCheckbox.Location = new System.Drawing.Point(136, 342);
            this.checkChecksumCheckbox.Name = "checkChecksumCheckbox";
            this.checkChecksumCheckbox.Size = new System.Drawing.Size(15, 14);
            this.checkChecksumCheckbox.TabIndex = 11;
            this.checkChecksumCheckbox.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 414);
            this.Controls.Add(this.encodingComboBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkChecksumCheckbox);
            this.Controls.Add(this.darkThemeCheckBox);
            this.Controls.Add(this.connectOnStartupCheckBox);
            this.Controls.Add(this.patronPinTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.patronBarcodeTextBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.itemBarcodeTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.vendorProfileTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.apTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.aoTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.hostnameTextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SettingsForm";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox hostnameTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox portTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox aoTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox apTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox usernameTextBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox passwordTextBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox vendorProfileTextBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox patronBarcodeTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox patronPinTextBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox itemBarcodeTextBox;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.CheckBox connectOnStartupCheckBox;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.CheckBox darkThemeCheckBox;
        private System.Windows.Forms.ComboBox encodingComboBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkChecksumCheckbox;
    }
}