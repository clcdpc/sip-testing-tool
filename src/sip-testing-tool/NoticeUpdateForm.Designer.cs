﻿namespace Clc.Sip
{
    partial class NoticeUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.noticeMediumComboBox = new System.Windows.Forms.ComboBox();
            this.deliveryDateTextBox = new System.Windows.Forms.TextBox();
            this.patronBarcodeTextBox = new System.Windows.Forms.TextBox();
            this.itemBarcodeTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.noticeStatusComboBox = new System.Windows.Forms.ComboBox();
            this.notificationTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(79, 204);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Update Notice";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // noticeMediumComboBox
            // 
            this.noticeMediumComboBox.FormattingEnabled = true;
            this.noticeMediumComboBox.Location = new System.Drawing.Point(101, 66);
            this.noticeMediumComboBox.Name = "noticeMediumComboBox";
            this.noticeMediumComboBox.Size = new System.Drawing.Size(121, 21);
            this.noticeMediumComboBox.TabIndex = 1;
            // 
            // deliveryDateTextBox
            // 
            this.deliveryDateTextBox.Location = new System.Drawing.Point(101, 40);
            this.deliveryDateTextBox.Name = "deliveryDateTextBox";
            this.deliveryDateTextBox.Size = new System.Drawing.Size(121, 20);
            this.deliveryDateTextBox.TabIndex = 2;
            // 
            // patronBarcodeTextBox
            // 
            this.patronBarcodeTextBox.Location = new System.Drawing.Point(101, 120);
            this.patronBarcodeTextBox.Name = "patronBarcodeTextBox";
            this.patronBarcodeTextBox.Size = new System.Drawing.Size(121, 20);
            this.patronBarcodeTextBox.TabIndex = 3;
            // 
            // itemBarcodeTextBox
            // 
            this.itemBarcodeTextBox.Location = new System.Drawing.Point(101, 146);
            this.itemBarcodeTextBox.Name = "itemBarcodeTextBox";
            this.itemBarcodeTextBox.Size = new System.Drawing.Size(121, 20);
            this.itemBarcodeTextBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Patron Barcode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Item Barcode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Delivery Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Notice Medium";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "NotificationType";
            // 
            // noticeStatusComboBox
            // 
            this.noticeStatusComboBox.FormattingEnabled = true;
            this.noticeStatusComboBox.Location = new System.Drawing.Point(101, 13);
            this.noticeStatusComboBox.Name = "noticeStatusComboBox";
            this.noticeStatusComboBox.Size = new System.Drawing.Size(121, 21);
            this.noticeStatusComboBox.TabIndex = 10;
            // 
            // notificationTypeComboBox
            // 
            this.notificationTypeComboBox.FormattingEnabled = true;
            this.notificationTypeComboBox.Location = new System.Drawing.Point(101, 93);
            this.notificationTypeComboBox.Name = "notificationTypeComboBox";
            this.notificationTypeComboBox.Size = new System.Drawing.Size(121, 21);
            this.notificationTypeComboBox.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Notice Status";
            // 
            // commentTextBox
            // 
            this.commentTextBox.Location = new System.Drawing.Point(101, 172);
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.Size = new System.Drawing.Size(121, 20);
            this.commentTextBox.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(44, 175);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Comment";
            // 
            // NoticeUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(233, 244);
            this.Controls.Add(this.notificationTypeComboBox);
            this.Controls.Add(this.noticeStatusComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.commentTextBox);
            this.Controls.Add(this.itemBarcodeTextBox);
            this.Controls.Add(this.patronBarcodeTextBox);
            this.Controls.Add(this.deliveryDateTextBox);
            this.Controls.Add(this.noticeMediumComboBox);
            this.Controls.Add(this.button1);
            this.Name = "NoticeUpdateForm";
            this.Text = "NoticeUpdate";
            this.Load += new System.EventHandler(this.NoticeUpdateForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox noticeMediumComboBox;
        private System.Windows.Forms.TextBox deliveryDateTextBox;
        private System.Windows.Forms.TextBox patronBarcodeTextBox;
        private System.Windows.Forms.TextBox itemBarcodeTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox noticeStatusComboBox;
        private System.Windows.Forms.ComboBox notificationTypeComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.Label label7;
    }
}