﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clc.Sip
{
    public enum NoticeMedium
    {
        AnyOrUnknown = 000,
        Mail = 001,
        Phone = 002,
        Fax = 003,
        Email = 004,
        SMS = 005
    }
}
