﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sip_net_core
{
    public enum NoticeStatus
    {
        UnsuccessfulDelivery = 00,
        SuccessfulDelivery = 01,
        InterceptedRejected = 02
    }
}
