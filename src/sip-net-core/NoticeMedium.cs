﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sip_net_core
{
    public enum NoticeMedium
    {
        AnyOrUnknown = 000,
        Mail = 001,
        Phone = 002,
        Fax = 003,
        Email = 004,
        SMS = 005
    }
}
